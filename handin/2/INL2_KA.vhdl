library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity INL2_KA is
	port(
		A : in std_logic;
		B : in std_logic;
		Y : out std_logic);
end INL2_KA;

architecture KA of INL2_KA is
begin
  process (A,B)
    variable temp_A : std_logic;
    variable temp_B : std_logic;
    begin
      if ((A = 'H') or (A = '1') or (A = 'L') or (A = '0')) and
        ((B = 'H') or (B = '1') or (B = 'L') or (B = '0')) then
        if (A = 'H') then
          temp_A := '1';
        elsif (A = 'L') then
          temp_A := '0';
        else 
          temp_A := A;
        end if;
        if (B = 'H') then
          temp_B := '1';
        elsif (B = 'L') then
          temp_B := '0';
        else 
          temp_B := B;
        end if; 
        Y <= (not (temp_A and temp_B));
      else 
        Y <= 'X';
      end if;
    end process;
end KA;