entity INL2_KB is
	port(
		D : in bit_vector(2 downto 0);
		C : in bit;
		S : in bit;
		Q : out bit_vector(2 downto 0));
end INL2_KB;

architecture KB of INL2_KB is
	signal O : bit_vector(2 downto 0);
begin
	Q <= O;
	process (D,C,S)
	begin
		if (S = '1') then
			O <= "111";
		elsif (S = '0') then
			-- if a positive edge on C
			if (C'event and (C = '1')) then
				for i in 0 to 2 loop
					O(i) <= not D(i);
				end loop;
			end if;
		end if;

	end process;
end KB;