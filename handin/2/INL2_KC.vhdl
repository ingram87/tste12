entity INL2_KC is
	port(
		A : in bit;
		B : in bit;
		C : in bit;
		Q : out bit);
end INL2_KC;

architecture KC of INL2_KC is
begin
	process (A,B,C)
		variable temp : bit;
	begin
		-- if a falling edge on C
		if (C'event and (C = '0')) then
			if (A = '0') and (B = '0') then
				temp := '0';
			elsif (A = '1') and (B = '1') then
				temp := not temp;
			else
				temp := temp;
			end if;
		end if;
		Q <= temp;
	end process;
end KC;