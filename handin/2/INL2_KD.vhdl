library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity INL2_KD is
	generic (Limit : integer := 5);
	port(
		A : in integer;
		Y : out std_logic);
end INL2_KD;

architecture KD of INL2_KD is 
begin
	process(A)
	begin
 		if (A > Limit) then
 			Y <= '1';
 		elsif (A = Limit) then
 			Y <= 'X';
 		elsif (A < Limit) then
 			Y <= '0';
 		end if;
	end process;
end KD;