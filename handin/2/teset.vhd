entity teset is
  
  port (
    A : in  bit;
    Y : out bit);

end teset;

architecture KA of INL1_KA is

begin  -- KA

  Y <= A;

end KA;