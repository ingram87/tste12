library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

package INL3_KA is
	function KA (A : std_logic_vector) return integer;
end INL3_KA;

package body INL3_KA is

	function KA (A : std_logic_vector) return integer is
		variable result : integer;
	begin
		
		for i in A'range loop

			if (A(i) = '0') or (A(i) = '0') or (A(i) = '0') or (A(i) = '0') then
				result := result + 1;
			end if;
		end loop;

		return result;
	
	end function;

end INL3_KA;