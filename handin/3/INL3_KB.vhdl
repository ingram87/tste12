entity INL3_KB is
	port(
		C : in bit;
		S : in bit;
		Q : out bit_vector(2 downto 0));
end INL3_KB;

architecture KB of INL3_KB is
	variable temp : bit_vector(2 downto 0);
begin
	temp := Q;

	if (S = '1') then
		Q <= "000";
	elsif (S = '0') and (C'event) and (C = '1') then

		if (temp = "000") then
			temp := "111";
		else
			temp := temp - 1;
		end if;

		Q <= temp;

	end if;

end KB;