entity INL4_KA is
  
  generic (
    C_time : time := 4ns);

  port (
    A : in bit_vector(1 to 2);
    B : in bit;
    Y : out bit);
end INL4_KA;

architecture KA of INL4_KA is
  signal oldA : bit_vector(1 to 2);
  signal newA : bit_vector(1 to 2);
begin  -- KA
  
  trig : process(B)
  begin
    if B'Event and B = '1' then
      oldA <= A;
      newA <= A after C_time;
    end if;
  end process;

  stable : process(A, B)
  begin
    if oldA = newA then
      Y <= transport '1' after 2ns;
    end if;
  end process;
  

end KA;
