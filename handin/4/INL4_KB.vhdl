library ieee;
use ieee.std_logic_1164.all;

entity INL4_KB is
  
  port (
    C : in bit;                         -- clock
    R : in bit;                         -- synchronous reset 
    E : in bit;                         -- asynchronous output enable
    Q : inout std_logic_vector(2 downto 1));   

end INL4_KB;

architecture KB of INL4_KB is
  signal reg : std_logic_vector(2 downto 1) := "ZZ";
begin  -- KB

  tristateRegister: process (C, R, E, Q)
  begin  -- process tristateRegister
    if E = '1' then                     -- an asynchronous output

      for I in 1 to 2 loop              -- output the Q with reg value
        Q(I) <= reg(I);
      end loop;  -- I
    
    else
      Q <= "ZZ";                        -- otherwise output "ZZ" on Q
    end if;
    
    if C'event and C = '0' then      -- if there is a falling edge on C
      if R = '1' then                   -- synchronous reset
        reg <= "00";                      -- set "00" on reg
      elsif R = '0' then
        for I in 1 to 2 loop            -- copy the value from Q to reg
          reg(I) <= Q(I);
        end loop;  -- I
      end if;
    end if;
  end process tristateRegister;
  
end KB;
