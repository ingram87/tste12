library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity INL4_KC is
  
  port (
    A : in  bit_vector(3 downto 0);
    B : in  bit_vector(3 downto 0);
    C : in  bit_vector(7 downto 0);
    Y : out bit_vector(8 downto 0));

end INL4_KC;

architecture KC of INL4_KC is
begin  -- KC

  -- purpose: Multiply-Accumulate-unit
  -- type   : combinational
  -- inputs : A, B, C
  -- outputs: Y
  MAC: process (A, B, C)
  
  variable IA : integer;
  variable IB : integer;
  variable IC : integer;
  variable Ires : integer;
  variable Sres : signed(8 downto 0);
  variable SA : signed(3 downto 0);  -- signed version of A
  variable SB : signed(3 downto 0);  -- signed version of B
  variable SC : signed(7 downto 0);  -- signed version of C
  
  begin  -- process MAC
    for I in 0 to 3 loop                -- duplicate A
      if A(I) = '0' then
        SA(I) := '0';
      elsif A(I) = '1' then
        SA(I) := '1';
      end if;
    end loop;  -- I
    
    for I in 0 to 3 loop                -- duplicate B
      if B(I) = '0' then
        SB(I) := '0';
      elsif B(I) = '1' then
        SB(I) := '1';
      end if;
    end loop;  -- I
    
    
    for I in 0 to 7 loop                -- duplicate C
      if C(I) = '0' then
        SC(I) := '0';
      elsif C(I) = '1' then
        SC(I) := '1';
      end if;
    end loop;  -- I
   
    IA := Conv_INTEGER(SA);
    IB := Conv_INTEGER(SB);
    IC := Conv_INTEGER(SC);
    Ires := IA * IB + IC;
    Sres := Conv_SIGNED(Ires, 9);            -- A * B + C 
    
    for I in 0 to 8 loop                -- copy the result to Y
      if Sres(I) = '0' then
        Y(I) <= '0';
      elsif Sres(I) = '1' then
        Y(I) <= '1';
      end if;
    end loop;  -- I
      
  
  end process MAC;

  

end KC;
