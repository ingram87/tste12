library ieee;
use ieee.numeric_std.all;

entity INL3_KB is
	port(
		C : in bit;
		S : in bit;
		Q : out bit_vector(2 downto 0));
end INL3_KB;

architecture KB of INL3_KB is
  
  -- lookup table
  type lut is array (0 to 7) of bit_vector(2 downto 0);
  constant my_lut : lut := (
  0 => "000",
  1 => "001",
  2 => "010",
  3 => "011",
  4 => "100",
  5 => "101",
  6 => "110",
  7 => "111");

begin
  cnt : process (C,S)
    variable index : integer := 0;
    begin
      -- asynchronous reset
      if S = '1' then
        Q <= my_lut(0);
        index := 0;
        
      -- on each rising_edge of C and S = 0
      elsif S = '0' and C'event and C = '1' then
        if (index = 0) then
          Q <= my_lut(0);
          index := 7;
        else
          Q <= my_lut(index);
          index := index - 1;
		    end if;
	    end if;
	
    end process;

end KB;