entity INL3_KC is
	port(
		A : in bit;
		B : in bit;
		Y : out bit);
end INL3_KC;

architecture KC of INL3_KC is
begin
  transportdelay : process(A,B)
    variable temp : bit;
    variable old_temp : bit;
  begin
    temp := A or B;
    
    if (temp /= old_temp) and (temp = '1') then
      old_temp := '1';
      Y <= transport temp after 3 ns;
    elsif (temp /= old_temp) and (temp = '0') then
      old_temp := '0';
      Y <= transport temp after 4 ns;
    end if;
  end process;
  	
end KC;