entity INL3_KD is
	port(
		A : in bit;
		B : in bit;
		Y : out bit);
end INL3_KD;

architecture KD of INL3_KD is
  component INL3_KD_dummy
    port (
      E1 : in bit;
      E2 : in bit;
      EY : out bit);
  end component;
  
  signal Y_temp1, Y_temp2 : bit;
  
begin
  
  U1 : INL3_KD_dummy port map (
    E1 => A,
    E2 => B,
    EY => Y_temp1);
  -- test
  
  Y <= not Y_temp1;
      
end KD;