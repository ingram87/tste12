library IEEE;
use ieee.std_logic_1164.all;
use work.inl3_ka.all;

entity test is
	port(
	  clk : in std_logic;
	  rst : in std_logic;
		Y : out integer);
end entity;


architecture behav of test is
begin
	process (clk)
		variable A : std_logic_vector(7 downto 0);
	begin
	  if (rst = '1') then
	    A := "1WULUWH1";
	    Y <= 0;
		elsif (rising_edge(clk)) then
		  Y <= KA(A);
		end if;
	end process;
		
end behav;