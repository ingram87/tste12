--
-- VHDL Architecture KEYBOARD.keyboard_signals.behav
--
-- Created:
--          by - liji050.student (egypten-11.edu.isy.liu.se)
--             - matfe759.student 
--          at - 12:26:45 09/08/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

ENTITY keyboard_signals IS
   PORT( 
      kb_clk  : IN     std_logic;
      kb_data : IN     std_logic;
      sys_clk : IN     std_logic;
      Hex6    : OUT    std_logic_vector (6 DOWNTO 0);
      Hex7    : OUT    std_logic_vector (6 DOWNTO 0);
      db      : OUT    std_logic_vector (9 DOWNTO 0);
      rsb     : OUT    std_logic_vector (6 DOWNTO 0)
   );

-- Declarations

END keyboard_signals ;

--
ARCHITECTURE behav OF keyboard_signals IS
shared variable shift_register : STD_LOGIC_VECTOR(9 downto 0);
shared variable first_time : std_logic := '1';
shared variable data, clock, old_clk_value : std_logic;
shared variable edge_found : std_logic := '0';
shared variable bit_cnt : STD_LOGIC_VECTOR(3 downto 0) := "0000";
shared variable kb_register : STD_LOGIC_VECTOR(9 downto 0);
shared variable one_cnt : integer := 0;


-- Constants
constant KEY1_SCAN : std_logic_vector(7 DOWNTO 0) := "00010110";
constant KEY2_SCAN : std_logic_vector(7 DOWNTO 0) := "00011110";
constant KEY3_SCAN : std_logic_vector(7 DOWNTO 0) := "00100110";
constant KEY4_SCAN : std_logic_vector(7 DOWNTO 0) := "00100101";
constant KEY5_SCAN : std_logic_vector(7 DOWNTO 0) := "00101110";
constant KEY6_SCAN : std_logic_vector(7 DOWNTO 0) := "00110110";
constant KEY7_SCAN : std_logic_vector(7 DOWNTO 0) := "00111101";
constant KEY8_SCAN : std_logic_vector(7 DOWNTO 0) := "00111110";
constant KEY9_SCAN : std_logic_vector(7 DOWNTO 0) := "01000110";
constant KEY0_SCAN : std_logic_vector(7 DOWNTO 0) := "01000101";

constant KEY1_rsb : std_logic_vector(7 downto 0) := "1111001";
constant KEY2_rsb : std_logic_vector(7 downto 0) := "0100100";
constant KEY3_rsb : std_logic_vector(7 downto 0) := "0110000";
constant KEY4_rsb : std_logic_vector(7 downto 0) := "0011001";
constant KEY5_rsb : std_logic_vector(7 downto 0) := "0010010";
constant KEY6_rsb : std_logic_vector(7 downto 0) := "0000010";
constant KEY7_rsb : std_logic_vector(7 downto 0) := "1111000";
constant KEY8_rsb : std_logic_vector(7 downto 0) := "0000000";
constant KEY9_rsb : std_logic_vector(7 downto 0) := "0010000";
constant KEY0_rsb : std_logic_vector(7 downto 0) := "1000000";
constant ELSE_rsb : std_logic_vector(7 downto 0) := "0000110";

--

-- Synchronize the external inputs (kb_data and kb_clk)
procedure sync_keyboard
(keyboard_data : in std_logic;
keyboard_clock : in std_logic) is
begin
  data := keyboard_data;
  clock := keyboard_clock;
end sync_keyboard;

-- creates an output (edge_found) at falling_edge of synced kb_clk
-- via one system clk
procedure detect_falling_kb_clk
(keyboard_clock : in std_logic) is
begin
  if (first_time = '1') then
    first_time := '0';
    old_clk_value := keyboard_clock;
  elsif ((keyboard_clock = '0') and (old_clk_value = '1')) then
    edge_found := '1';
    old_clk_value := clock;
  elsif (edge_found = '1') then
    edge_found := '0';
    old_clk_value := clock;
  else
    old_clk_value := clock;
  end if;
end detect_falling_kb_clk;

procedure convert_scancode is
begin
  if (edge_found = '1') then
    -- if it is a start bit?
    if ((bit_cnt = "0000") and (data = '0')) then
      shift_register(0) := shift_register(1);
      shift_register(1) := shift_register(2);
      shift_register(2) := shift_register(3);
      shift_register(3) := shift_register(4);
      shift_register(4) := shift_register(5);
      shift_register(5) := shift_register(6);
      shift_register(6) := shift_register(7);
      shift_register(7) := shift_register(8);
      shift_register(8) := shift_register(9);
      shift_register(9) := data;
      bit_cnt := bit_cnt + 1;
    elsif (bit_cnt > "0000") then
      shift_register(0) := shift_register(1);
      shift_register(1) := shift_register(2);
      shift_register(2) := shift_register(3);
      shift_register(3) := shift_register(4);
      shift_register(4) := shift_register(5);
      shift_register(5) := shift_register(6);
      shift_register(6) := shift_register(7);
      shift_register(7) := shift_register(8);
      shift_register(8) := shift_register(9);
      shift_register(9) := data;
      bit_cnt := bit_cnt + 1;
    end if;
  end if;
  
  if (bit_cnt = "1011") then
    bit_cnt := "0000";
      
    -- counts how many '1' in shift_register without stop bit
    for i in 0 to 8 loop
      if (shift_register(i) = '1') then
        one_cnt := one_cnt + 1;
      end if;
    end loop;
    
    -- check odd_parity bit and Stop bit
    if (((one_cnt mod 2)=1) and
        (shift_register(9) = '1')) then
      for i in 0 to 9 loop
        kb_register(i) := shift_register(i);
      end loop;
    else 
      for i in 0 to 9 loop
        kb_register(i) := 'X';
      end loop; 
    end if;
  end if;
      
    
end convert_scancode;

-- Convert the keys' scancodes to rsb for LED
procedure scancode_to_rsb is
BEGIN
  for i in 0 to 9 loop
    case kb_register(i) is
      when KEY1_SCAN => rsb(i) <= KEY1_rsb;
      when KEY2_SCAN => rsb(i) <= KEY2_rsb;
      when KEY3_SCAN => rsb(i) <= KEY3_rsb;
      when KEY4_SCAN => rsb(i) <= KEY4_rsb;
      when KEY5_SCAN => rsb(i) <= KEY5_rsb;
      when KEY6_SCAN => rsb(i) <= KEY6_rsb;
      when KEY7_SCAN => rsb(i) <= KEY7_rsb;
      when KEY8_SCAN => rsb(i) <= KEY8_rsb;
      when KEY9_SCAN => rsb(i) <= KEY9_rsb;
      when KEY0_SCAN => rsb(i) <= KEY0_rsb;
      when OTHERS => rsb(i) <= OTHERS_rsb;
    end case;
  end loop;
end scancode_translate;

-- Display group number
procedure display_gnr is
BEGIN
-- "1" 
-- "7"
end display_gnr;

BEGIN
  process(sys_clk)
  begin
    -- if we detect a rising_edge signal, then we read the synced clk&data from keyboard
    if (rising_edge(sys_clk)) then
      sync_keyboard(kb_data, kb_clk);
      -- if a falling edge is detected, it means a data bit is coming in.
      detect_falling_kb_clk(clock);
      -- if it is a falling edge?    
      convert_scancode;
    end if;
    
    for i in 0 to 9 loop
      db(i) <= kb_register(i);
    end loop;


    
  end process;
END ARCHITECTURE behav;

