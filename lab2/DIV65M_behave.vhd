--
-- VHDL Architecture lab2_lib.DIV65M.behave
--
-- Created:
--          by - matfe759.student (olympen-13.edu.isy.liu.se)
--          at - 19:32:22 09/24/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
LIBRARY altera_mf;
USE altera_mf.all;
USE ieee.std_logic_unsigned.all;

ENTITY DIV65M IS
   PORT( 
      vga_clk_signal : IN     std_logic;
      fpga_reset     : IN     std_logic;
      GLED           : OUT    std_logic
   );

-- Declarations

END DIV65M ;

--
ARCHITECTURE behave OF DIV65M IS
BEGIN
  process(vga_clk_signal)
    variable temp : std_logic_vector(25 downto 0) := (others => '0');
  begin
    if (fpga_reset = '1') then
      GLED <= '0';
    elsif (fpga_reset = '0') then
      if (rising_edge(vga_clk_signal)) then
        if (temp = "1111011111110100100011111") then
          temp := (others => '0');
          GLED <= not GLED;
        else 
          temp := temp + 1;
        end if;
      end if;
    end if;
  end process;
    
END ARCHITECTURE behave;

