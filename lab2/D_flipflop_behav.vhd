--
-- VHDL Architecture lab2_lib.D_flipflop.behav
--
-- Created:
--          by - liji050.student (egypten-15.edu.isy.liu.se)
--          at - 14:57:44 09/25/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
LIBRARY altera_mf;
USE altera_mf.all;
USE ieee.std_logic_unsigned.all;

ENTITY D_flipflop IS
   PORT( 
      vga_clk     : IN     std_logic;
      vga_hsync_n : OUT    std_logic;
      vga_vsync_n : OUT    std_logic;
      vga_blank   : IN     std_logic;
      vga_hsync   : IN     std_logic;
      vga_vsync   : IN     std_logic;
      vga_blank_n : OUT    std_logic
   );

-- Declarations

END D_flipflop ;

--
ARCHITECTURE behav OF D_flipflop IS
BEGIN
  -- with blank=no delay, v,s = 1 delay, left border missing yellow and lower border missing red
  -- with blank= 2 delays, v,s = 0 delay, left border missing all hte colors
  vga_blank_n <= vga_blank;
  vga_vsync_n <= vga_vsync;
  vga_hsync_n <= vga_hsync;
  
  
--  blank : process (vga_blank, vga_clk)
--    variable b_a : std_logic;
--    variable b_b : std_logic;
--  begin
--    if (rising_edge(vga_clk)) then
--      vga_blank_n <= b_b;
--      b_b := b_a;
--      b_a := vga_blank;
--    end if;
--  end process;
--  
  --vsync : process (vga_vsync, vga_clk)
--    variable v_a : std_logic;
--    --variable v_b : std_logic;
--  begin
--    if (rising_edge(vga_clk)) then
--      vga_vsync_n <= v_a;
--      v_a := vga_vsync;
--    end if;
--  end process;
--  
--  hsync : process (vga_hsync, vga_clk)
--    variable h_a : std_logic;
--    --variable h_b : std_logic;
--  begin
--    if (rising_edge(vga_clk)) then
--      vga_hsync_n <= h_a;
--      h_a := vga_hsync;
--    end if;
--  end process;
  
  
END ARCHITECTURE behav;

