--
-- VHDL Architecture lab2_lib.Graphic_generator.behav
--
-- Created:
--          by - liji050.student (asgard-02.edu.isy.liu.se)
--          at - 08:50:43 09/23/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;


ENTITY Graphic_generator IS
  PORT( 
      vga_clk_signal      : IN     std_logic;
      fpga_reset   : IN     std_logic;
      sram_ce_n    : OUT    std_logic;
      sram_ub_n    : OUT    std_logic;
      sram_lb_n    : OUT    std_logic;
      sram_oe_n    : OUT    std_logic;
      sram_we_n    : OUT    std_logic;
      vga_blank_n  : OUT    std_logic;
      HEX6         : OUT    std_logic_vector (6 DOWNTO 0);
      HEX7         : OUT    std_logic_vector (6 DOWNTO 0);
      sram_address : OUT    std_logic_vector (19 DOWNTO 0);
      vga_b        : OUT    std_logic_vector (7 DOWNTO 0);
      vga_g        : OUT    std_logic_vector (7 DOWNTO 0);
      vga_hsync_n  : OUT    std_logic;
      vga_r        : OUT    std_logic_vector (7 DOWNTO 0);
      vga_vsync_n  : OUT    std_logic;
      vga_sync     : OUT    std_logic;
      sram_data    : IN     std_logic_vector (15 DOWNTO 0)
   );
END ENTITY Graphic_generator;

--
ARCHITECTURE behav OF Graphic_generator IS
BEGIN
END ARCHITECTURE behav;

