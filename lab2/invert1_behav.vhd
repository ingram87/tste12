--
-- VHDL Architecture lab2_lib.invert1.behav
--
-- Created:
--          by - liji050.student (egypten-15.edu.isy.liu.se)
--          at - 19:53:24 09/25/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
LIBRARY altera_mf;
USE altera_mf.all;
USE ieee.std_logic_unsigned.all;

ENTITY invert1 IS
-- Declarations
  port (
    raw_vga : in std_logic;
    vga_clk_signal : out std_logic);
END invert1 ;

--
ARCHITECTURE behav OF invert1 IS
BEGIN
  vga_clk_signal <= not raw_vga;
END ARCHITECTURE behav;

