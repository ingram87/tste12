--
-- VHDL Architecture lab2_lib.invert.behav
--
-- Created:
--          by - liji050.student (asgard-02.edu.isy.liu.se)
--          at - 09:09:05 09/23/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

ENTITY invert IS
   PORT( 
      fpga_reset_n : IN     std_logic;
      fpga_reset   : OUT    std_logic
   );

-- Declarations

END invert ;

--
ARCHITECTURE behav OF invert IS
BEGIN
  fpga_reset <= not fpga_reset_n;
END ARCHITECTURE behav;

