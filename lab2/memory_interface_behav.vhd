--
-- VHDL Architecture lab2_lib.memory_interface.behav
--
-- Created:
--          by - liji050.student (asgard-02.edu.isy.liu.se)
--          at - 09:00:36 09/23/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

ENTITY memory_interface IS
   PORT( 
      fpga_reset     : IN     std_logic;
      sram_address   : OUT    std_logic_vector (19 DOWNTO 0);
      sram_ce_n      : OUT    std_logic;
      sram_lb_n      : OUT    std_logic;
      sram_oe_n      : OUT    std_logic;
      sram_ub_n      : OUT    std_logic;
      sram_we_n      : OUT    std_logic;
      sram_data      : IN     std_logic_vector (15 DOWNTO 0);
      hblank         : IN     std_logic;
      vblank         : IN     std_logic;
      vga_clk_signal : IN     std_logic;
      pix_register   : OUT    std_logic_vector(15 DOWNTO 0)
   );

-- Declarations

END memory_interface;

--
ARCHITECTURE behave OF memory_interface IS
BEGIN
  sram_we_n <= '1';
  sram_oe_n <= '0';
  sram_ce_n <= '0';
  sram_lb_n <= '0';
  sram_ub_n <= '0';
  -- if a delay added here, the aboving border and left border will disappear, WHICH IS NOT RIGHT
  pix_register <= sram_data;
  
  SRAM : process(vga_clk_signal, fpga_reset)
    variable c : integer;
  begin
    if (fpga_reset = '1') then
      sram_address <= (others => '0');
    elsif (rising_edge(vga_clk_signal)) then
      if (hblank = '0') then
        sram_address <= sram_address + 1;
      elsif (vblank = '1') then
        sram_address <= (others => '0');
      end if;
    end if;
  end process;
END ARCHITECTURE behave;

