--
-- VHDL Architecture lab2_lib.Memory_interface.behav
--
-- Created:
--          by - matfe759.student (egypten-12.edu.isy.liu.se)
--          at - 14:57:04 09/17/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--



--The Memory interface shall produce the SRAM address and control signals

-- process pixrg The 16-bit register that stores the pixel received from the SRAM.

-- process SRAM_control


LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

ENTITY Memory_interface IS
-- Declarations

END Memory_interface ;

--
ARCHITECTURE behav OF Memory_interface IS
BEGIN
END ARCHITECTURE behav;

