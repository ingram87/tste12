--
-- VHDL Architecture lab2_lib.VGA_controller.behav
--
-- Created:
--          by - matfe759.student (egypten-12.edu.isy.liu.se)
--          at - 14:57:57 09/17/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--

--while VGA_controller generates monitor control signals and
--translates the incoming sram_data into red, green and blue color data

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

ENTITY VGA_controller IS
  port (
    vga_clk : in std_logic;
    sram_data : in std_logic_vector(15 downto 0);
    fpga_reset_n : in std_logic;
    vga_blank_n : out std_logic;
    vga_r : out std_logic_vector(7 downto 0);
    vga_g : out std_logic(7 downto 0);
    vga_b : out std_logic(7 downto 0);
    vga_hsync_n : out std_logic;
    vga_vsync_n : out std_logic;
    HEX6 : out std_logic_vector(6 downto 0);
    HEX7 : out std_logic_vector(6 downto 0));
    
-- Declarations

-- => incoming SRAM data => output red, green, blue data
-- => hblank, vblank, pblank
-- => outgoing signals hcnt, vcnt,  to memory_interface
-- process pixelcounter
-- process linecounter
-- process hsyncr
-- process vsyncr
-- process blank_syncr

---------------------------------------------

-- process vga_gen
--for line_cnt=1 to L
--/* send L lines of video to the monitor */
--for pixel_cnt=1 to P
--/* send P pixels for each line */
--data = RAM(address)
--/* get pixel data from the memory */
--address = address + 1
--/* FLASH data word contains 4 pixels */
--color = COLOR_MAP(data)
--/* get the color for the right-bit pixel */
--send color to monitor
--pixel_cnt = pixel_cnt + 1
--for horiz_blank_cnt=1 to H
--/* blank the monitor for H pixels */
--color = BLANK
--send color to monitor
--/* pulse the horizontal sync at the right time */
--if horiz_blank_cnt>HB0 and horiz_blank_cnt<HB1
--hsync = 0
--else
--hsync = 1
--horiz_blank_cnt = horiz_blank_cnt + 1
--line_cnt = line_cnt + 1
--for vert_blank_cnt=1 to V
--/* blank the monitor for V lines and insert vertical sync */
--color = BLANK
--send color to monitor
--/* pulse the vertical sync at the right time */
--if vert_blank_cnt>VB0 and vert_blank_cnt<VB1
--vsync = 0
--else
--vsync = 1
--vert_blank_cnt = vert_blank_cnt + 1
--/* go back to start of picture in memory */
--address = 0


------------------------------------------------------------------------


END VGA_controller ;

--
ARCHITECTURE behav OF VGA_controller IS
signal pix_cnt : std_logic_vector(10 downto 0) := "00000000000";
signal lin_cnt : std_logic_vector(9 downto 0) := "0000000000";
signal hsync_cnt : std_logic_vector(10 downto 0) := "00000000000";

--hsyncr:s signaler
signal h_blank : std_logic := '0';
signal pix_trig : std_logic := '0';

signal v_blank : std_logic := '0';

------------------------------------------------------------------------
-- CONSTANTS
------------------------------------------------------------------------
-- VGA clock on 65MHz, which gives us a clock period T of 15.38ns
-- the resolution we choose is:
-- XGA(60Hz) 1024x768

-- 1 pixel per 1343 vga_clock
constant max_pix_cnt : std_logic_vector(10 downto 0) := "10100111111";
constant max_lin_cnt : std_logic_vector(9 downto 0) := "1100100101";
constant max_hsync_cnt : std_logic_vector(10 downto 0) := "10000000000";

-- total number of visible columns (c: 15.8us)
constant HDISPLAY : std_logic_vector(10 downto 0) := "10000000011"; -- 1027
-- value for the horizontal counter for front porch (d: 0.4us, 26clk)
constant HFP   : std_logic_vector(10 downto 0) := "10000011101"; -- 1053
-- value for the horizontal counter for the synch pulse (a: 2.1us, 136clk)
constant HSP   : std_logic_vector(10 downto 0) := "10010100110"; -- 1190
-- value for the horizontal counter for back porch (b: 2.5us, 163clk)
constant HBP   : std_logic_vector(10 downto 0) := "10101001001"; -- 1353

-- total number of visible lines (c: 768)
constant VDISPLAY : std_logic_vector(9 downto 0) := "1100000000"; -- 768
-- value for the vertical counter for the front porch (d: 3)
constant VFP   : std_logic_vector(9 downto 0) := "1100000011"; -- 771
-- value for the vertical counter for the synch pulse (a: 6)
constant VSP   : std_logic_vector(9 downto 0) := "1100001001"; -- 777
-- value for the vertical counter for back porch (b: 29)
constant VBP   : std_logic_vector(9 downto 0) := "1100100110"; -- 806


BEGIN
  
  pixelcounter : process(vga_clk,FPGA_reset_n)
    begin
      if FPGA_reset_n='1' then
        pix_cnt <= "00000000000";
      elsif(rising_edge(vga_clk)) then
        if pix_cnt=max_pix_cnt then
          pix_cnt<="00000000000";
        else
          pix_cnt <= pix_cnt + 1;
        end if;
      end if;
  end process;
  
  linecounter : process(vga_hsync_n,FPGA_reset_n)
    begin
      if FPGA_reset_n='1' then
        lin_cnt <= "0000000000";
      elsif(rising_edge(vga_hsync_n)) then
        if lin_cnt=max_lin_cnt then
          lin_cnt<="0000000000";
        else
          lin_cnt <= lin_cnt + 1;
        end if;
      end if;
  end process;
  
  hsyncr : process(vga_clk, FPGA_reset_n, pix_cnt)
    variable h_cnt : std_logic_vector(10 downto 0);
  begin
    -- on every vga clock
    if (rising_edge(vga_clk)) then
      -- main:
      -- within the display area (a)
      if (h_cnt < HDISPLAY) then
        h_cnt <= h_cnt + 1;
      -- send the BLANK signal when approaching blank area
      elsif (h_cnt = HDISPLAY) then
        h_blank <= '1';
        h_cnt <= h_cnt + 1;
      -- between the display and front porch (d)
      elsif (h_cnt > HDISPLAY) and (h_cnt < HFP) then
        h_cnt <= h_cnt + 1;
      -- lower the h_sync when sync pulse time starts
      elsif (h_cnt = HFP) then
        vga_hsync_n <= '0';
        h_cnt <= h_cnt + 1;
      -- pulse period
      elsif (h_cnt > HFP) and (h_cnt < HSP) then
        h_cnt <= h_cnt + 1;
      -- higher the h_sync when sync pulse time ends
      elsif (h_cnt = HSP) then
        vga_hsync_n <= '1';
        h_blank <= '0';
        h_cnt <= h_cnt + 1;
      -- between back porch and display starts
      elsif (h_cnt > HBP) then
        h_cnt <= h_cnt + 1;
      -- reset the counter when reach max 
      elsif (h_cnt = HBP) then
        h_cnt <= "00000000000";
      end if;
    end if;
  end process;

  vsyncr : process(vga_clk, FPGA_reset_n, pix_cnt)
    variable v_cnt : std_logic_vector(9 downto 0);
  begin
    -- on every vga clock
    if (rising_edge(vga_clk)) then
      -- main:
      -- within the display area (a)
      if (v_cnt < VDISPLAY) then
        v_cnt <= v_cnt + 1;
      -- send the BLANK signal when approaching blank area
      elsif (v_cnt = VDISPLAY) then
        v_blank <= '1';
        v_cnt <= v_cnt + 1;
      -- between the display and front porch (d)
      elsif (v_cnt > VDISPLAY) and (v_cnt < VFP) then
        v_cnt <= v_cnt + 1;
      -- lower the v_sync when sync pulse time starts
      elsif (v_cnt = VFP) then
        vga_vsync_n <= '0';
        v_cnt <= v_cnt + 1;
      -- pulse period
      elsif (v_cnt > VFP) and (v_cnt < VSP) then
        v_cnt <= v_cnt + 1;
      -- higher the v_sync when sync pulse time ends
      elsif (v_cnt = VSP) then
        vga_vsync_n <= '1';
        v_blank <= '0';
        v_cnt <= v_cnt + 1;
      -- between back porch and display starts
      elsif (v_cnt > VBP) then
        v_cnt <= v_cnt + 1;
      -- reset the counter when reach max 
      elsif (v_cnt = VBP) then
        v_cnt <= "0000000000";
      end if;
    end if;
  end process;


END ARCHITECTURE behav;









