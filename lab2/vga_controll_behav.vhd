--
-- VHDL Architecture lab2_lib.vga_control_test.behave
--
-- Created:
--          by - matfe759.student (egypten-09.edu.isy.liu.se)
--          at - 15:16:30 09/24/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

ENTITY vga_controll IS
   PORT( 
      hblank         : OUT    std_logic;
      HEX6           : OUT    std_logic_vector (6 DOWNTO 0);
      HEX7           : OUT    std_logic_vector (6 DOWNTO 0);
      vga_b          : OUT    std_logic_vector (7 DOWNTO 0);
      vga_sync       : OUT    std_logic;
      vga_r          : OUT    std_logic_vector (7 DOWNTO 0);
      vga_g          : OUT    std_logic_vector (7 DOWNTO 0);
      fpga_reset     : IN     std_logic;
      vblank         : OUT    std_logic;
      pix_register   : IN     std_logic_vector (15 DOWNTO 0);
      vga_clk_signal : IN     std_logic;
      vga_vsync      : OUT    std_logic;
      vga_blank      : OUT    std_logic;
      vga_clk        : OUT    std_logic;
      vga_hsync      : OUT    std_logic
   );

-- Declarations

END vga_controll ;

--
ARCHITECTURE behave OF vga_controll IS
  signal pix_cnt : std_logic_vector(10 downto 0);
  signal lin_cnt : std_logic_vector(9 downto 0);
 
  --hsyncr:s signaler
  signal h_blank : std_logic;
  signal pix_trig : std_logic;
  signal v_blank : std_logic;
  
  ------------------------------------------------------------------------
  -- CONSTANTS
  ------------------------------------------------------------------------
  -- VGA clock on 65MHz, which gives us a clock period T of 15.38ns
  -- the resolution we choose is:
  -- XGA(60Hz) 1024x768
  
  constant HO : std_logic_vector(10 downto 0) := "00000000000"; -- 0
  -- value for the horizontal counter for the synch pulse (a: 2.1us, 136clk)
  constant HA   : std_logic_vector(10 downto 0) := "00010000111"; -- 136
  --constant HA   : std_logic_vector(10 downto 0) := "00010001000"; -- 136 
  -- value for the horizontal counter for front porch (b: 2.46us, 160clk)
  constant HB   : std_logic_vector(10 downto 0) := "00100100111"; -- 296
  -- total number of visible columns (c: 15.753846154us, 1024clk) ca 15.75us
  constant HC : std_logic_vector(10 downto 0) := "10100101000"; -- 1320
  -- value for the horizontal counter for back porch (d: 0.369230769 ca 0.37us, 24clk)
  constant HD   : std_logic_vector(10 downto 0) := "10100111111"; --1343
    
  constant VO : std_logic_vector(9 downto 0) := "0000000000"; -- 0
  -- value for the vertical counter for the synch pulse (a: 6)
  constant VA   : std_logic_vector(9 downto 0) := "0000000110"; -- 6
  -- value for the vertical counter for the front porch (b: 29)
  constant VB   : std_logic_vector(9 downto 0) := "0000100011"; -- 35
  -- total number of visible lines (c: 768)
  constant VC : std_logic_vector(9 downto 0) := "1100100011"; -- 803
  -- value for the vertical counter for back porch (d: 3)
  constant VD   : std_logic_vector(9 downto 0) := "1100100101"; -- 805
  
BEGIN
  vga_clk <= vga_clk_signal;
  vga_sync <= '0';
  HEX7 <= "1111001";
  HEX6 <= "1111000";
  
  pixelcounter : process(vga_clk_signal,fpga_reset)
    begin
      if fpga_reset='1' then
         pix_cnt <= (others => '0');
      elsif(rising_edge(vga_clk_signal)) then
         if(pix_cnt = HD) then
          pix_cnt <= (others => '0');
         else
          pix_cnt <= pix_cnt + 1;
         end if;
      end if;
  end process;
  
  linecounter : process(vga_clk_signal,fpga_reset)
    begin
      if fpga_reset='1' then
        lin_cnt <= (others => '0');
      elsif (rising_edge(vga_clk_signal)) then
        if (lin_cnt = VD) then
          lin_cnt <= (others => '0');
        elsif(pix_cnt = HA) then
          lin_cnt <= lin_cnt + 1;
        end if;
      end if;
  end process;
  

  hsyncr : process(vga_clk_signal, fpga_reset)
  begin
    if (fpga_reset = '1') then
      hblank <= '1';
      h_blank <= '1';
      vga_hsync <= '0';
    -- on every vga clock
    elsif (rising_edge(vga_clk_signal)) then
      -- main:
      -- between 0 and a:
      if (pix_cnt > HO) and (pix_cnt < HA) then
        vga_hsync <= '0';
      elsif (pix_cnt = HA) then
        vga_hsync <= '1';
      elsif (pix_cnt = HB) then
        h_blank <= '0';
        hblank <= '0';
      elsif (pix_cnt = HC) then
        h_blank <= '1';
        hblank <= '1';
      elsif (pix_cnt = HD) then
        vga_hsync <= '0';
      end if; 
    end if;
  end process;
  
  vsyncr : process(vga_clk_signal, fpga_reset)
  begin
    -- on every vga clock
    if (fpga_reset = '1') then
      vblank <= '1';
      v_blank <= '1';
      vga_vsync <= '0';
   
   elsif (rising_edge(vga_clk_signal)) then
      -- main:
      if (lin_cnt > VO) and (lin_cnt < VA) then
        vga_vsync <= '0';
      elsif (lin_cnt = VA) then
        vga_vsync <= '1';  
      elsif (lin_cnt = VB) then
        v_blank <= '0';
        vblank <= '0';
      elsif (lin_cnt = VC) then
        v_blank <= '1';
        vblank <= '1';
      elsif (lin_cnt = VD) then
        vga_vsync <= '0';
      end if;      
    end if;
  end process;
  
  blank_syncr : process(vga_clk_signal, fpga_reset)
  begin
    if (fpga_reset = '1') then
      vga_blank <= '1';
    elsif (rising_edge(vga_clk_signal)) then
      vga_blank <= not(h_blank or v_blank);
    end if;
  end process;
  
  vga_gen : process(vga_clk_signal, fpga_reset)
  begin
    if (fpga_reset = '1') then
      vga_r(7 downto 0) <= (others => '0');
      vga_g(7 downto 0) <= (others => '0');
      vga_b(7 downto 0) <= (others => '0');
    -- Control if we are blanking (=0) or not (=1)
    elsif (h_blank = '0' and v_blank = '0') then
      vga_r(7 downto 2) <= pix_register(15 downto 10);
      vga_r(1 downto 0) <= (others => '0');
      vga_g(7 downto 3) <= pix_register(9 downto 5);
      vga_g(2 downto 0) <= (others => '0');
      vga_b(7 downto 3) <= pix_register(4 downto 0);
      vga_b(2 downto 0) <= (others => '0');
--      vga_r(7 downto 2) <= pix_register(15 downto 10);
--      vga_r(1 downto 0) <= (others => '0');
--      vga_g(7 downto 3) <= pix_register(9 downto 5);
--      vga_g(2 downto 0) <= (others => '0');
--      vga_b(7 downto 3) <= pix_register(4 downto 0);
--      vga_b(2 downto 0) <= (others => '0');
    elsif (h_blank = '1' or v_blank = '1') then
      vga_r(7 downto 0) <= (others => '0');
      vga_g(7 downto 0) <= (others => '0');
      vga_b(7 downto 0) <= (others => '0');
    end if;
  end process;
END ARCHITECTURE behave;

