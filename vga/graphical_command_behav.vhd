--
-- VHDL Architecture soundflux_lib.graphical_command.behav
--
-- Created:
--          by - liji050.student (egypten-15.edu.isy.liu.se)
--          at - 10:14:01 10/02/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
LIBRARY soundflux_lib;
USE soundflux_lib.datatypes.all;
USE IEEE.std_logic_unsigned.all;

ENTITY graphical_command IS
   PORT( 
      pixel_register : OUT    std_logic_vector (15 DOWNTO 0);
      vol            : IN     unsigned (3 DOWNTO 0);
      eco_vol        : IN     unsigned (3 DOWNTO 0);
      balance        : IN     unsigned (3 DOWNTO 0);
      eco_delay      : IN     unsigned (3 DOWNTO 0);
      reset_n        : IN     std_logic;
      fpga_clk_65M   : IN     std_logic;
      vcnt           : IN     std_logic_vector (9 DOWNTO 0);
      hcnt           : IN     std_logic_vector (10 DOWNTO 0);
      gc_enable      : IN     std_logic
   );

-- Declarations

END graphical_command ;

--
ARCHITECTURE behav OF graphical_command IS
 
  -- Coordinates for Panel box border --
  -- (X1,Y1)            (X2,Y1) --
  --     -----------------      --
  --     --             --      --
  --     --             --      --
  --     -----------------      --
  -- (X1,Y2)            (X2,Y2) --
  --------------------------------------
  --constant panel_x1 : std_logic_vector(10 downto 0);
--  constant panel_x2 : std_logic_vector(10 downto 0);
--  constant panel_y1 : std_logic_vector(9 downto 0);
--  constant panel_y2 : std_logic_vector(9 downto 0);
  
  -- 10 levels of sound wave strength, can be implememted in many ways
  -- e.g. Balance, Volumn etc.
  signal wave_strength : std_logic_vector (4 downto 0);

  -- ColorMap
  -- (red, green, blue)
  -- White: FF, FF, FF
  constant white : std_logic_vector (15 downto 0) := "1111111111111111";
  -- Black: 00, 00, 00
  constant black : std_logic_vector (15 downto 0) := "0000000000000000";
  -- Red: FF, 00, 00
  constant red : std_logic_vector (15 downto 0) := "1111110000000000";
  -- Green: 00, FF, 00
  constant green : std_logic_vector (15 downto 0) := "0000001111100000";
  -- Blue: 00, 00, FF
  constant blue : std_logic_vector (15 downto 0) := "0000000000011111";
  -- Yellow: FF, FF, 00
  constant yellow : std_logic_vector (15 downto 0) := "0000000000011111";
  -- Cyan: 00, FF, FF
  constant cyan : std_logic_vector (15 downto 0) := "0000001111111111";

  -- booleans for checking the type of the input command
  signal if_balance : bit := '0';
  signal if_ecodelay : bit := '0';
  signal if_ecovol : bit := '0';
  signal if_vol : bit := '0';


--  procedure draw_rectangle (signal xx : integer; signal xy : integer; signal yx : integer; signal yy : integer; color : std_logic_vector(15 downto 0)) is
--  begin
--    if (pixel_cnt > panel_x1) and (pixel_cnt < panel_x2) and (line_cnt > panel_y1) and (line_cnt > panel_y2) then
--      pixel_register <= color;
--    end if;
--  end draw_rectangle;
  
begin
  -- a init process, set all the variables
  init : process(fpga_clk_65M, reset_n)
  begin
  end process;
  
  -- read the commands and set up all the variables/signals
  command_input : process (fpga_clk_65M, reset_n)
  begin
  end process;

  -- send the pix shape to the vga_controller
  send_pix : process (fpga_clk_65M, reset_n)
  BEGIN
    -- start to create pix pic
    --if (if_balance = '1') then
      -- TODO: implement
      -- an example of drawing a panel box
      --draw_rectangle (panel_x1, panel_x2, panel_y1, panel_y2, red);
    --elsif (if_vol = '1') then
--    elsif (if_ecovol = '1') then
--    elsif (if_ecodelay = '1') then
--    else
    --end if;
  end process;

END ARCHITECTURE behav;

