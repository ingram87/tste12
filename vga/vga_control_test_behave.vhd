--
-- VHDL Architecture lab2_lib.vga_control_test.behave
--
-- Created:
--          by - matfe759.student (egypten-09.edu.isy.liu.se)
--          at - 15:16:30 09/24/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
--LIBRARY altera_mf;
--USE altera_mf.all;
USE ieee.std_logic_unsigned.all;

ENTITY vga_control_test IS
   PORT( 
      hblank            : OUT    std_logic;
      HEX6              : OUT    std_logic_vector (6 DOWNTO 0);
      HEX7              : OUT    std_logic_vector (6 DOWNTO 0);
      vga_b             : OUT    std_logic_vector (7 DOWNTO 0);
      vga_sync          : OUT    std_logic;
      vga_r             : OUT    std_logic_vector (7 DOWNTO 0);
      vga_g             : OUT    std_logic_vector (7 DOWNTO 0);
      fpga_reset        : IN     std_logic;
      vblank            : OUT    std_logic;
      vga_clk           : OUT    std_logic;
      pix_register      : IN     std_logic_vector (15 DOWNTO 0);
      vga_clk_signal    : IN     std_logic;
      vga_blank         : OUT    std_logic;
      vga_hsync         : OUT    std_logic;
      vga_vsync         : OUT    std_logic;
      push_button_two   : IN     std_logic;
      push_button_three : IN     std_logic;
      push_button_four  : IN     std_logic
   );

-- Declarations

END vga_control_test ;

--
ARCHITECTURE behave OF vga_control_test IS
  signal pix_cnt : std_logic_vector(10 downto 0);
  signal lin_cnt : std_logic_vector(9 downto 0);
 
  --hsyncr:s signaler
  signal h_blank : std_logic;
  signal pix_trig : std_logic;
  signal v_blank : std_logic;
  
  ------------------------------------------------------------------------
  -- CONSTANTS
  ------------------------------------------------------------------------
  -- VGA clock on 65MHz, which gives us a clock period T of 15.38ns
  -- the resolution we choose is:
  -- XGA(60Hz) 1024x768
  
  constant HO : std_logic_vector(10 downto 0) := "00000000000"; -- 0
  -- value for the horizontal counter for the synch pulse (a: 2.1us, 136clk)
  constant HA   : std_logic_vector(10 downto 0) := "00010000111"; -- 136
  --constant HA   : std_logic_vector(10 downto 0) := "00010001000"; -- 136 
  -- value for the horizontal counter for front porch (b: 2.46us, 160clk)
  constant HB   : std_logic_vector(10 downto 0) := "00100101000"; -- 296
  -- total number of visible columns (c: 15.753846154us, 1024clk) ca 15.75us
  constant HC : std_logic_vector(10 downto 0) := "10100101000"; -- 1320
  -- value for the horizontal counter for back porch (d: 0.369230769 ca 0.37us, 24clk)
  constant HD   : std_logic_vector(10 downto 0) := "10100111111"; --1343
    
  constant VO : std_logic_vector(9 downto 0) := "0000000000"; -- 0
  -- value for the vertical counter for the synch pulse (a: 6)
  constant VA   : std_logic_vector(9 downto 0) := "0000000110"; -- 6
  -- value for the vertical counter for the front porch (b: 29)
  constant VB   : std_logic_vector(9 downto 0) := "0000100011"; -- 35
  -- total number of visible lines (c: 768)
  constant VC : std_logic_vector(9 downto 0) := "1100100011"; -- 803
  -- value for the vertical counter for back porch (d: 3)
  constant VD   : std_logic_vector(9 downto 0) := "1100100101"; -- 805
  
  
  
  -- a temp signal for command
  signal command : std_logic;
  signal temp_pix_register : std_logic_vector(15 downto 0);
  
  -- implements a lookup table
  signal i : integer range 0 to 3:=0; -- change the range value
  type lut is array ( 0 to 2**2 - 1) of std_logic_vector(15 downto 0);
  constant my_lut : lut := (
    0 => "0000000000000000",
    1 => "1010100000000000",
    2 => "0000001010100000",
    3 => "0000000000010101"); 
  
  
  
  
  
  
BEGIN
  vga_clk <= vga_clk_signal;
  vga_sync <= '0';
  HEX7 <= "1111001";
  HEX6 <= "1111000";
  
  pixelcounter : process(vga_clk_signal,fpga_reset)
    begin
      if fpga_reset='1' then
         pix_cnt <= (others => '0');
      elsif(rising_edge(vga_clk_signal)) then
         if(pix_cnt = HD) then
          pix_cnt <= (others => '0');
         else
          pix_cnt <= pix_cnt + 1;
         end if;
      end if;
  end process;
  
  linecounter : process(vga_clk_signal,fpga_reset)
    begin
      if fpga_reset='1' then
        lin_cnt <= (others => '0');
      elsif (rising_edge(vga_clk_signal)) then
        if (lin_cnt = VD) then
          lin_cnt <= (others => '0');
        elsif(pix_cnt = HA) then
          lin_cnt <= lin_cnt + 1;
        end if;
      end if;
  end process;
  

  hsyncr : process(vga_clk_signal, fpga_reset)
  begin
    if (fpga_reset = '1') then
      hblank <= '1';
      h_blank <= '1';
      vga_hsync <= '0';
    -- on every vga clock
    elsif (rising_edge(vga_clk_signal)) then
      -- main:
      -- between 0 and a:
      if (pix_cnt > HO) and (pix_cnt < HA) then
        vga_hsync <= '0';
      elsif (pix_cnt = HA) then
        vga_hsync <= '1';
      elsif (pix_cnt = HB) then
        h_blank <= '0';
        hblank <= '0';
      elsif (pix_cnt = HC) then
        h_blank <= '1';
        hblank <= '1';
      elsif (pix_cnt = HD) then
        vga_hsync <= '0';
      end if; 
    end if;
  end process;
  
  vsyncr : process(vga_clk_signal, fpga_reset)
  begin
    -- on every vga clock
    if (fpga_reset = '1') then
      vblank <= '1';
      v_blank <= '1';
      vga_vsync <= '0';
   
   elsif (rising_edge(vga_clk_signal)) then
      -- main:
      if (lin_cnt > VO) and (lin_cnt < VA) then
        vga_vsync <= '0';
      elsif (lin_cnt = VA) then
        vga_vsync <= '1';  
      elsif (lin_cnt = VB) then
        v_blank <= '0';
        vblank <= '0';
      elsif (lin_cnt = VC) then
        v_blank <= '1';
        vblank <= '1';
      elsif (lin_cnt = VD) then
        vga_vsync <= '0';
      end if;      
    end if;
  end process;
  
  blank_syncr : process(vga_clk_signal, fpga_reset)
  begin
    if (fpga_reset = '1') then
      vga_blank <= '1';
    --elsif (rising_edge(vga_clk_signal)) then
      --vga_blank <= not(h_blank or v_blank);
    end if;
    vga_blank <= not(h_blank or v_blank);
  end process;
  
  
  
  
  -- assign command boolea and load up the corresponding colors when certain button is pressed
  alut : process(vga_clk_signal)
  begin
    if rising_edge(vga_clk_signal) then
      if push_button_two = '0' then
        command <= '1';
       temp_pix_register <= my_lut(1);
      elsif push_button_three = '0' then
        command <= '1';
        temp_pix_register <= my_lut(2);
      elsif push_button_four = '0' then
        command <= '1';
        temp_pix_register <= my_lut(3);
      else
        command <= '0';
        temp_pix_register <= (others => '0');
      end if;
    end if;
  end process;
    
  
  vga_gen : process(vga_clk_signal, fpga_reset)
  begin
    if (fpga_reset = '1') then
      vga_r(7 downto 0) <= (others => '0');
      vga_g(7 downto 0) <= (others => '0');
      vga_b(7 downto 0) <= (others => '0');
    -- Control if we are blanking (=0) or not (=1)
    elsif (h_blank = '0' and v_blank = '0' and command = '0') then
      
      -- title
      if (pix_cnt > 424) and (pix_cnt < 808) and (lin_cnt > 96) and (lin_cnt <315) then
        
        vga_r(7 downto 2) <= pix_register(15 downto 10);
        vga_r(1 downto 0) <= (others => '0');
        vga_g(7 downto 3) <= pix_register(9 downto 5);
        vga_g(2 downto 0) <= (others => '0');
        vga_b(7 downto 3) <= pix_register(4 downto 0);
        vga_b(2 downto 0) <= (others => '0');
      elsif (pix_cnt > 424) and (pix_cnt < 936) and (lin_cnt > 419) and (lin_cnt < 707) then
        vga_r(7 downto 2) <= pix_register(15 downto 10);
        vga_r(1 downto 0) <= (others => '0');
        vga_g(7 downto 3) <= pix_register(9 downto 5);
        vga_g(2 downto 0) <= (others => '0');
        vga_b(7 downto 3) <= pix_register(4 downto 0);
        vga_b(2 downto 0) <= (others => '0');
      else
        vga_r(7 downto 2) <= pix_register(15 downto 10);
        vga_r(1 downto 0) <= (others => '0');
        vga_g(7 downto 3) <= pix_register(9 downto 5);
        vga_g(2 downto 0) <= (others => '0');
        vga_b(7 downto 3) <= pix_register(4 downto 0);
        vga_b(2 downto 0) <= (others => '0');
      end if;
    elsif (h_blank = '0' and v_blank = '0' and command = '1') then
      
      -- just to test if the command works :)
      if (lin_cnt < 400) and (pix_cnt < 600) then
        vga_r(7 downto 2) <= temp_pix_register(15 downto 10);
        vga_r(1 downto 0) <= (others => '0');
        vga_g(7 downto 3) <= temp_pix_register(9 downto 5);
        vga_g(2 downto 0) <= (others => '0');
        vga_b(7 downto 3) <= temp_pix_register(4 downto 0);
        vga_b(2 downto 0) <= (others => '0');
      else      
        vga_r(7 downto 2) <= pix_register(15 downto 10);
        vga_r(1 downto 0) <= (others => '0');
        vga_g(7 downto 3) <= pix_register(9 downto 5);
        vga_g(2 downto 0) <= (others => '0');
        vga_b(7 downto 3) <= pix_register(4 downto 0);
        vga_b(2 downto 0) <= (others => '0');
      end if;
      
    elsif (h_blank = '1' or v_blank = '1') then
      vga_r(7 downto 0) <= (others => '0');
      vga_g(7 downto 0) <= (others => '0');
      vga_b(7 downto 0) <= (others => '0');
    end if;
  end process;
END ARCHITECTURE behave;

