--
-- VHDL Architecture soundflux_lib.vga_interface.behav
--
-- Created:
--          by - liji050.student (olympen-03.edu.isy.liu.se)
--          at - 15:59:11 10/03/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

ENTITY vga_interface IS
-- Declarations

END vga_interface ;

--
ARCHITECTURE behav OF vga_interface IS
BEGIN
  pix_register <= sample;
  
  temp : process(vga_clk_signal)
  variable c: integer;
  begin
    if (fpga_reset = '1') then
      sample <= (others => '0');
    elsif (rising_edge(vga_clk_signal)) then
      if hblank = '0' then
        sample <= sample + 1;
      elsif vblank = '1' then
        sample <= (others => '0');
      end if;
    end if;
  end process;
END ARCHITECTURE behav;

